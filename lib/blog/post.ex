defmodule Blog.Post do
  defstruct filename: nil, slug: nil, href: nil, meta: nil, title: nil,
            published_date: nil, markdown: nil, html: nil
end
