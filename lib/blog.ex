defmodule Blog do
  alias Blog.Post

  def main do
    post_files = Path.wildcard("./posts/*.md")
    layout_html = File.read!("./layout.html.eex")
    archive_html = File.read!("./archive.html.eex")

    clean_site_directory()

    sorted_posts =
      post_files
      |> Enum.map(fn (f) -> post_from_file(f, layout_html) end)
      |> sort_by_published_date

    sorted_posts |> Enum.each(&save_post/1)
    sorted_posts |> List.first |> save_index
    sorted_posts |> generate_archive(archive_html) |> save_archive(layout_html)

    File.copy!("./styles.css", "./site/styles.css")

    :ok
  end

  def clean_site_directory do
    File.rm_rf!("./site")
    File.mkdir_p!("./site/posts")
  end

  def post_from_file(filename, layout_html) do
    filename
    |> set_filename
    |> set_slug
    |> set_href
    |> set_meta_and_markdown
    |> set_title
    |> set_published_date
    |> set_html(layout_html)
  end

  def save_post(%Post{slug: slug, html: html}) do
    File.write!("./site/posts/#{slug}.html", html)
  end

  def save_archive(archive, layout_html) do
    File.write!("./site/archive.html",
                EEx.eval_string(layout_html, content: archive))
  end

  def generate_archive(posts, archive_html) do
    EEx.eval_string(archive_html, posts: posts)
  end

  def save_index(%Post{html: html}) do
    !File.write("./site/index.html", html)
  end

  def sort_by_published_date(posts) do
    Enum.sort(posts, fn (p1, p2) ->
      compare_dates(p1.published_date, p2.published_date)
    end)
  end

  def set_html(%Post{markdown: markdown} = post, layout) do
    html = Earmark.as_html!(markdown)
    %Post{post | html: EEx.eval_string(layout, content: html)}
  end

  def set_published_date(%Post{meta: meta} = post) do
    %Post{post | published_date: extract_published_date_from_meta(meta)}
  end

  def set_title(%Post{meta: meta} = post) do
    %Post{post | title: extract_title_from_meta(meta)}
  end

  def set_meta_and_markdown(%Post{filename: filename} = post) do
    contents = File.read!("./posts/#{filename}")
    [meta, markdown] = String.split(contents, "\n\n")
    %Post{post | meta: meta, markdown: markdown}
  end

  def set_href(%Post{slug: slug} = post) do
    %Post{post | href: "/posts/#{slug}.html"}
  end

  def set_slug(%Post{filename: filename} = post) do
    %Post{post | slug: Path.basename(filename, ".md")}
  end

  def set_filename(filename) do
    %Post{filename: Path.basename(filename)}
  end

  def compare_dates(date1, date2) do
    case Date.compare(date1, date2) do
      :lt -> false
      :gt -> true
      :eq -> true
    end
  end

  def extract_title_from_meta(meta) do
    [_match, title] = Regex.run(~r/^title: (.+)$/m, meta)
    title
  end

  def extract_published_date_from_meta(meta) do
    [_match, published] = Regex.run(~r/^published: (.+)$/m, meta)
    Date.from_iso8601!(published)
  end
end
