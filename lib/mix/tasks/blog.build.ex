defmodule Mix.Tasks.Blog.Build do
  use Mix.Task

  @shortdoc "Build the blog site"

  def run(_) do
    Blog.main

    IO.puts("Done!")
  end
end
