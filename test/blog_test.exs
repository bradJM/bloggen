defmodule BlogTest do
  use ExUnit.Case, async: true

  alias Blog.Post

  setup do
    {:ok,
     meta: "title: Hello World\npublished: 2017-02-24",
     markdown: "Hello, world!"}
  end

  test "extract_title_from_meta returns the correct title", %{meta: meta} do
    assert "Hello World" == Blog.extract_title_from_meta(meta)
  end

  test "extract_published_date_from_meta returns the correct date", %{meta: meta} do
    assert ~D[2017-02-24] == Blog.extract_published_date_from_meta(meta)
  end

  test "compare_dates works within the same month" do
    assert Blog.compare_dates(~D[2017-02-24], ~D[2017-02-01])
  end

  test "compare_dates works across months" do
    assert Blog.compare_dates(~D[2017-02-24], ~D[2017-01-14])
  end

  test "compare_dates works across years" do
    assert Blog.compare_dates(~D[2017-02-24], ~D[2016-08-22])
  end

  test "set_filename populates filename" do
    expected_post = %Post{filename: "hello-world.md"}
    assert expected_post == Blog.set_filename("hello-world.md")
  end

  test "set_slug populates slug" do
    post = %Post{filename: "hello-world.md"}
    expected_post = %Post{filename: "hello-world.md", slug: "hello-world"}
    assert expected_post == Blog.set_slug(post)
  end

  test "set_href populates href" do
    post = %Post{slug: "hello-world"}
    expected_post = %Post{slug: "hello-world", href: "/posts/hello-world.html"}
    assert expected_post == Blog.set_href(post)
  end

  test "set_title populates title", %{meta: meta} do
    post = %Post{meta: meta}
    expected_post = %Post{meta: meta, title: "Hello World"}
    assert expected_post == Blog.set_title(post)
  end

  test "set_published_date populates published_date", %{meta: meta} do
    post = %Post{meta: meta}
    expected_post = %Post{meta: meta, published_date: ~D[2017-02-24]}
    assert expected_post == Blog.set_published_date(post)
  end

  test "set_html populates html", %{markdown: markdown} do
    layout_html = "<%= content %>"
    post = %Post{markdown: markdown}
    expected_post = %Post{markdown: markdown, html: "<p>Hello, world!</p>\n"}
    assert expected_post == Blog.set_html(post, layout_html)
  end
end
